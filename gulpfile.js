const gulp        = require('gulp');
const browserSync = require('browser-sync').create();
const sass        = require('gulp-sass');
const uglify      = require('gulp-uglify-es').default;
const concat      = require('gulp-concat');
// Compile Sass & Inject Into Browser
gulp.task('sass', function() {
    return gulp.src(['scss/*.scss'])
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(gulp.dest("css"))
        .pipe(browserSync.stream());
});
// Watch Scripts
gulp.task('scripts', function() {
    return gulp.src(['dev/js/fontfaceobserver.standalone.js', 'dev/js/owl.carousel.min.js',
        'dev/js/util.js', 'dev/js/tab.js', 'dev/js/modal.js', 'dev/js/svgxuse.js', 
        'dev/js/main.js'])
        .pipe(concat('main.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('js'))
        .pipe(browserSync.stream());
});
// Watch Sass & Serve
gulp.task('serve', ['sass','scripts'], function() {
        browserSync.init({
        proxy: "http://localhost/",
        notify: false
    });
    gulp.watch(['scss/*.scss'], ['sass']);
    gulp.watch(["dev/js/*.js"], ['scripts']);
    gulp.watch("*.php").on('change', browserSync.reload);
    
});

// Default Task

gulp.task('default', ['serve']);