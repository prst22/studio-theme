<?php 
    $categories = get_the_category(); 
    $post = get_queried_object();
    $postTypeName = get_post_type_object(get_post_type($post));
?>

<div class="position-relative main_post__heading">
    <h1 class="heading_title"><?php the_title(); ?></h1>
    <div class="heading_info">
        <span class="post_type"><b><?php if($postTypeName){echo esc_html($postTypeName->labels->name).'|';} ?></b></span>
        <?php 
            $separetor = ", ";
            $output = '';
            if($categories){
                foreach($categories as $category){
                    $output .= '<a href="'. get_category_link($category->term_id) . '">' . $category ->cat_name . '</a>' .$separetor;
                }
                echo trim($output, $separetor);
            }
            ?>

        <?php 
        the_time('F j, Y'); 
        if(is_singular( 'stories' )){
            print ' '.get_the_author_meta('first_name') .' '. get_the_author_meta('last_name'); 
        }?>
    </div>
    <!-- post image start-->
    <?php if(has_post_thumbnail()): ?>

    <div class="mb-5 <?php if(is_singular('stories')){ ?>story_main_image<?php }else{ ?>post_main_image<?php } ?>">

        <?php if(is_singular('stories')){
            the_post_thumbnail('single-story-thumnail', 
            $attr = array(
                'alt'   => "post main image",
                'class' => "story_main_image__image"
            )); 

        }else{
            the_post_thumbnail('large-client-thumnail', 
            $attr = array(
                'alt'   => "post main image",
                'class' => "post_main_image__image"
            )); 
        }
        
            ?>
    </div>

    <?php endif; ?>
    <!-- post image end -->
    <div class="position-absolute d-xl-flex flex-column align-items-end heading_info_md">
        <span class="post_type"><b><?php if($postTypeName){echo esc_html($postTypeName->labels->name);} ?></b></span>
        <span>
            <b>
                <?php 
                if(is_singular( 'stories' )){
                    $authorName = get_the_author_meta('first_name') .' '. get_the_author_meta('last_name');
                    if(strlen($authorName) > 1){
                        echo $authorName;
                    }
                }
                ?> 
            </b>
        </span>
        
            <?php  
            $outputForLargeScreens = '';
            if($categories){
                foreach($categories as $category){
                    $outputForLargeScreens .= '<span><a href="'. get_category_link($category->term_id) . '">' . $category ->cat_name . '</a></span>';
                }
                echo trim($outputForLargeScreens, $separetor);
            }
            ?>
        
        <span>
            <?php the_time('F j, Y'); ?>
        </span>
       
    </div>
</div>
<div class="main_post__content">
    <?php  the_content(); ?>
</div>