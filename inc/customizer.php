<?php  
	function studio_customizer_register($wp_customize){
		$wp_customize->add_section('video_slider',array(
			'title'				=> __('Video slider', 'studio'),
			'description'		=> sprintf(__('Home page video slider options', 'studio')),
			'priority'			=> 210
		));
		$wp_customize-> add_setting('video_link_one',array(
			'default'		=> _x('https://www.youtube.com/watch?v=TblZ0wQ1-B4', 'studio'),
			'type' 			=> 'theme_mod'
		));
		$wp_customize->add_control('video_link_one', array(
			'label'			=> __('Video link 1', 'sudio'),
			'section'		=> 'video_slider',
			'priority' 		=> 1
		));

		$wp_customize-> add_setting('video_link_two',array(
			'default'		=> _x('https://www.youtube.com/watch?v=Bey4XXJAqS8', 'studio'),
			'type' 			=> 'theme_mod'
		));
		$wp_customize->add_control('video_link_two', array(
			'label'			=> __('Video link 2', 'sudio'),
			'section'		=> 'video_slider',
			'priority' 		=> 2
		));

		$wp_customize-> add_setting('video_link_three',array(
			'default'		=> _x('https://www.youtube.com/watch?v=v87mUWsPyeg', 'studio'),
			'type' 			=> 'theme_mod'
		));
		$wp_customize->add_control('video_link_three', array(
			'label'			=> __('Video link 3', 'sudio'),
			'section'		=> 'video_slider',
			'priority' 		=> 3
		));

		$wp_customize-> add_setting('video_link_four',array(
			'default'		=> _x('https://www.youtube.com/watch?v=u3APNJYMrLo', 'studio'),
			'type' 			=> 'theme_mod'
		));
		$wp_customize->add_control('video_link_four', array(
			'label'			=> __('Video link 4', 'sudio'),
			'section'		=> 'video_slider',
			'priority' 		=> 4
		));

	}
	add_action('customize_register', 'studio_customizer_register');