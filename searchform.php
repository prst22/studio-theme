<form method="get" class="searchform form-inline my-lg-0 search_form" autocomplete="off" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<input type="text" class="search_field" name="s" id="s" placeholder="<?php esc_attr_e( 'Search' ); ?>" title="search"/>
	<button class="my-sm-0 search_btn" type="submit" name="submit" id="searchsubmit">
		<svg class="icon search"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#search"></use></svg>
	</button>
</form>