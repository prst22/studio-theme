<?php
/*
Template Name: All Stories
*/
?>
<?php
	get_header();
?>
<section class="block block--news_stories">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 col-md-4 px-0">
				<div class="position-relative news_stories_slider">
					<div class="owl-carousel">
						<div class="news_stories_slider__item">
							<img class="owl-lazy" data-src="<?php echo get_template_directory_uri(); ?>/images/stories_news_0.jpg" alt="picture">
							<div class="position-absolute news_stories_slider_overlay"></div>
						</div>
						<div class="news_stories_slider__item">
							<img class="owl-lazy" data-src="<?php echo get_template_directory_uri(); ?>/images/stories_news_1.jpg" alt="picture">
							<div class="position-absolute news_stories_slider_overlay"></div>
						</div>
						<div class="news_stories_slider__item">
							<img class="owl-lazy" data-src="<?php echo get_template_directory_uri(); ?>/images/stories_news_2.jpg" alt="picture">
							<div class="position-absolute news_stories_slider_overlay"></div>
						</div>
					</div>
					<div class="position-absolute w-100 d-inline-flex news_stories_slider_nav_cnt">
						<div class="d-flex justify-content-center align-items-center h-100 news_stories_slider_nav_cnt__inner"></div>
						<div class="d-flex justify-content-center align-items-center h-100 news_stories_slider_nav_cnt__inner_two">
							STORIES PAGE
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-8">
				<div class="container news_stories">
					<div class="row">
						<div class="col-12">
							<div class="news_stories__inner">
								<div class="container">
									<div class="row">
										<div class="col-12 px-0 pb-3"><h1>Stories</h1></div>
										<?php
											//Protect against arbitrary paged values
											$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

										    $args = array(
											'post_type' => 'stories',
											'posts_per_page' => 14,
											'paged' => $paged,
											'meta_query' => array( 
										        array(
										            'key' => '_thumbnail_id'
										        ) 
										    ),
										    'tax_query' => array( array(
									            'taxonomy' => 'post_format',
									            'field' => 'slug',
									            'terms' => 'post-format-video',
									           ) ),
											'update_post_meta_cache' => false, 
											'update_post_term_cache' => false  
										);
										$stories = new WP_Query( $args ); 
										if ( $stories->have_posts()) :
										 while ( $stories->have_posts()) : $stories->the_post(); ?>
										 	<?php 
										 		get_template_part( 'content', 'news_stories' );

											wp_reset_postdata();
										?>
										<?php endwhile;
										endif; // end of the loop. ?>
									</div>
									<div class="row">
										<div class="col-12 px-0 featured_story_cnt">
											<div class="featured_story_cnt__inner">
									   			<?php
													$args2 = array( 
														'post_type' => array('stories'),
														'posts_per_page' => -1,
														'meta_query' => array( 
													        array(
													            'key' => '_thumbnail_id'
													        ) 
													    ),
													    'no_found_rows' => true, 
														'update_post_meta_cache' => false, 
														'update_post_term_cache' => false,  
														'fields' => 'ids'
													 );
													$featuredLoop = new WP_Query( $args2 );
													if ($featuredLoop->have_posts()) : while ($featuredLoop->have_posts()) : $featuredLoop->the_post();?>

													<?php $featured = get_post_meta( get_the_ID(), 'studio_featured_1', true );
													    if($featured == 1 && (get_post_format() == 'video')): ?>
													     	<div class="d-flex flex-column-reverse flex-lg-row flex-nowrap">
													     		<div class="d-flex align-items-end featured">
													     			<a class="featured__link" href="<?php the_permalink(); ?>">

													     				<svg class="icon play"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#play"></use></svg>

														     			<h2 class="mb-3">
														     				<?php the_title();?> 
														     			</h2>

														     	        <span class="featured_date">
														     	        	<?php the_time('j F, Y'); ?>
														     	        		
														     	        </span>
														     	    </a>
													     		</div>
													     		<div class="position-relative featured_thumb">
													     			<?php the_post_thumbnail('medium-story-thumnail'); ?>
													     			<span class="position-absolute featured_thumb__lable">
													     				trending
													     			</span>
													     		</div>
													     	</div>
				
													    <?php 
														    break;
															endif; ?>												
												<?php  
												/* Restore original Post Data */
													wp_reset_postdata();
													endwhile;
													else: ?>
								                       <h1>No featured stories so far(</h1>
												<?php endif; ?>	
											</div>

										</div>
									</div>
									<?php if($stories->max_num_pages > 1): ?>	
										<div class="row">
											<div class="col-12 px-0 py-3">
												<div class="pagination_cnt">
													<div class="d-flex justify-content-between pagination_cnt__inner">
														<?php previous_posts_link( '<span class="iconslider icon-arrow-left2 px-1"></span>prev page ', $stories->max_num_pages); ?>
												        <?php next_posts_link( 'next page <span class="iconslider icon-arrow-right2 px-1"></span>', $stories->max_num_pages); ?>
													</div>
												</div>
											</div>
										</div>
									<?php endif; ?>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</section>
<section class="pt-0 block block--tabs_cnt">
	<div class="container pb-5 position-relative mx-auto tabs_inner">
	
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h1 class="position-relative text-center stories__header">Our Lates Clients</h1>
				</div>
			</div>
			<div class="row carousel_cnt carousel_cnt_all_news">
					<!-- loop start -->
					<?php
					    $args2 = array(
						'post_type' => 'studio_clients',
						'posts_per_page' => 8,
						'meta_query' => array( 
					        array(
					            'key' => '_thumbnail_id'
					        ) 
					    ),
					    'no_found_rows' => true, 
						'update_post_meta_cache' => false, 
						'update_post_term_cache' => false,  
						'fields' => 'ids'
					);

					$lastClients = new WP_Query( $args2 ); ?>

					<?php if ( $lastClients->have_posts() ) : ?>

						<div class="owl-carousel">
						

						<!-- the loop -->
						<?php while ( $lastClients->have_posts() ) : $lastClients->the_post(); ?>
							
								<figure class="mx-auto position-relative client_tab_prev">
									<span class="position-absolute client_tab_prev__go_to_client">
										<a href="<?php the_permalink(); ?>"><svg class="icon arrow-up-right2"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#arrow-up-right2"></use></svg></a>
									</span>
									<div class="client_tab_prev__image">
										<?php the_post_thumbnail('medium-client-thumnail');?>
									</div>
									<h3 class="position-absolute client_tab_prev__heading">
										<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
									</h3>
									<?php $description = get_post_meta( get_the_ID(), 'studio_text', true ); 
											if($description) : ?>
											<h6 class="position-absolute client_tab_prev__description">
											<?php echo $description; ?>
											</h6>
									<?php endif; ?>
								</figure>
								<?php  ?>
							
						<?php endwhile; ?>
						<!-- end of the loop -->
                        
						<!-- pagination here -->
						</div>

						<?php wp_reset_postdata(); ?>

					<?php else : ?>
						<p><?php esc_html_e( 'Sorry, no clients matched your criteria.' ); ?></p>
					<?php endif; ?>
					<!-- loop end -->
				
			</div>
		</div>
								
	</div>
</section>
	
<?php		
	get_footer();
?>