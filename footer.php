	<footer class="footer">
		<div class="footer__inner">
			<div class="d-flex justify-content-between w-100 top">
				<div class="footer-logo">
					<h1>LOGO</h1>
				</div> 

				<?php  if(is_active_sidebar('search')):?>
				<div class="position-relative search_cnt">
					<?php dynamic_sidebar('search'); ?>   
				</div>
				<?php endif; ?>
			</div>
			<div class="d-flex justify-content-between bottom">
				<div class="bottom__footer-copy">
					&copy; <?php echo date("Y") ;?> - <a href="<?php echo home_url();?>">studio.potatosites.com</a>	
				</div> 
				<div class="d-flex align-items-center bottom__social">
					<ul class="d-flex flex-wrap mb-0 vertical_social__list vertical_social__list--bottom">
						<li>
						    <a href="https://www.facebook.com/">
						    	<svg class="icon facebook2"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#facebook"></use></svg>
						    </a>
					    </li>
						<li>
						    <a href="https://www.instagram.com/">
						    	<svg class="icon instagram"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#instagram"></use></svg>
						    </a>
					    </li>
						<li>
						    <a href="https://www.youtube.com/">
						    	<svg class="icon youtube"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#youtube"></use></svg>
						    </a>
					    </li>
						<li>
						    <a href="https://twitter.com/"><svg class="icon twitter">
						    	<use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#twitter"></use></svg>
						    </a>
					    </li>
					</ul>
				</div>
			 </div>	
        </div>
	</footer>
</div>
<?php wp_footer(); ?>
</body>

</html>
